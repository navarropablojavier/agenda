require ('dotenv').config();

const PORT = process.env.PORT;
const bodyParser = require ('body-parser');
const fs = require ('fs');
const express = require ('express');

let file;

const loadFile = () => file = JSON.parse (fs.readFileSync(process.env.DB, 'utf-8'));
const saveFile = () => file = fs.writeFileSync(process.env.DB, JSON.stringify(File));

const app = express();

app.use(bodyParser.json({limit: '100MB'}));
app.use(bodyParser.urlencoded({}));

app.get('/', (req, res, next) => {
    loadFile();
    res.send(file);
});

app.get('/query', (req, res, next) => {res.send(req.query.id)});

app.get('/:id', (req, res, next) => {
    loadFile();
    const result = file.find(obj => parseInt(obj._id) === parseInt(req.params.id));
    res.send(result);
});


app.put('/:id', (req, res, next) =>{
    loadFile();
    const result = file.find(obj => parseInt(obj._id) === parseInt(req.params.id));
    Object.assign(result, req.body);
    saveFile();
    res.sendStatus(200);
});


app.delete('/:id', (req, res, next) =>{
    loadFile();
    const result = file.find (obj => parseInt(obj._id) === parseInt (req.params.id));
    Object.assign(result, {deleted: true, deletedAt: new Date});
    saveFile();
    res.sendStatus(200);
});

app.post('/', (req, res, next) => {
    try {
        loadFile();
        const object = req.body;
        const _id = file.length + 1;
        file.push ({
            ...object,
            _id
        });
        saveFile();
        res.send({success : true});
    } catch (err) {
        
    }
});


app.listen(PORT, () => console.log(`Runing on port ${PORT}`));